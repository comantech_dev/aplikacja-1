/*
 * Program transformujący dane wejściowe (z pliku test1.xlsx) do
 * listy obiektów typu Node(id: Int, name: String, nodes: List[Node]) 
 * Standardowe wyjście w formacie JSON. 
 */
package pl.comantech.xlstransform;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author Sebastian
 */
public class Application extends UnicastRemoteObject implements RmiServer {

    private static final long serialVersionUID = 1L;
    private static final int NUM_COLUMNS = 4;

    private List<Node> nodes;

    public Application() throws RemoteException {
        super();
    }

    @Override
    public List<Node> getNodes() {
        return nodes;
    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        try {
            LocateRegistry.createRegistry(1099);
            System.out.println("RMI registry created");
        } catch (RemoteException e) {
            System.out.println("RMI registry already exists");
        }

        Application application = new Application();
        application.transform("/files/test1.xlsx");
        // Bind this object instance to the name "TransformServer"
        Naming.rebind("//localhost/TransformServer", application);

    }

    /**
     * Transform XLSX file and display result
     *
     * @param fileName
     */
    public void transform(String fileName) {

        // Prepare Nodes from file
        nodes = getNodes(fileName);
        
        // Prepare message to display
        String message = prepareMessage(nodes, new ObjectMapper());
        
        // Display
        System.out.println(message);

    }

    /**
     * Prepare message to display with default pretty printer
     *
     * @param nodes
     * @param mapper
     * @return
     */
    private String prepareMessage(List<Node> nodes, ObjectMapper mapper) {

        StringBuilder json = new StringBuilder();
        try {
            for (Node node : nodes) {
                json.append(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(node));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return json.toString();
    }

    /**
     * Generate Nodes
     *
     * @param fileName
     * @return
     */
    private List<Node> getNodes(String fileName) {

        // Name as key, Objects as value
        Map<String, Node> tmpNodes = new TreeMap<>();

        try {

            XSSFWorkbook workbook = new XSSFWorkbook(getClass().getResourceAsStream(fileName));
            XSSFSheet sheet = workbook.getSheetAt(0);

            // Column's index for parent
            Integer parentIndex = 0;

            // Column's index as key, Node as value
            Map<Integer, Node> parents = new HashMap<>();

            //Iterate rows - one by one
            Iterator<Row> rows = sheet.iterator();

            while (rows.hasNext()) {

                Row row = rows.next();

                // Omit first row
                if (row.getRowNum() == 0) {
                    continue;
                }

                Node node = new Node();

                for (int column = 0; column < NUM_COLUMNS; column++) {

                    Cell cell = row.getCell(column);

                    if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

                        node.setName(cell.getStringCellValue());
                        tmpNodes.put(node.getName(), node);

                        // get parent
                        if (column > 0) {

                            Node parent = parents.get(column - 1);

                            // add child to parent
                            parent.getNodes().add(node);
                            if (tmpNodes.get(node.getName()) != null) {
                                // Only ROOT with childs (as A,B,C,D)
                                tmpNodes.remove(node.getName());
                            }
                        }

                        // 0 and 1 columns maybe as parent
                        if (column < 2) {

                            parentIndex = column;
                            parents.put(parentIndex, node);
                        }
                    }

                    if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {

                        Double id = cell.getNumericCellValue();
                        node.setId(id.intValue());
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList(tmpNodes.values());
    }

}
