package pl.comantech.xlstransform;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author Sebastian
 */
public interface RmiServer extends Remote {

    public List<Node> getNodes() throws RemoteException;
}
