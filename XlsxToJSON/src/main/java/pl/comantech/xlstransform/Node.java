package pl.comantech.xlstransform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastian
 */
public class Node implements Serializable{
    
    private static final long serialVersionUID = 1124574616911661470L;

    private Integer id;
    private String name;
    private List<Node> nodes = new ArrayList<>();

    public Node(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Node(String name) {
        this.name = name;
    }

    public Node() {
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the nodes
     */
    public List<Node> getNodes() {
        return nodes;
    }

    /**
     * @param nodes the nodes to set
     */
    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    @Override
    public String toString() {
        return "Node{" + "id=" + id + ", name=" + name + '}';
    }

}
