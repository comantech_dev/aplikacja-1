package pl.comantech.xlstransform;

import java.rmi.RemoteException;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author Sebastian
 */
public class ApplicationTest {

    /**
     * Test of Transform method, of class Application.
     *
     * @throws java.rmi.RemoteException
     */
    @Test
    public void testTransfrom() throws RemoteException {

        System.out.println("getNodesTest");

        Application application = new Application();
        application.transform("/files/test1.xlsx");

        List<Node> result = application.getNodes();

        Assert.assertNotNull("List must not be NULL", result);
        Assert.assertEquals("Wrong size", 4, result.size());

        Assert.assertEquals("Wrong value", "A", result.get(0).getName());
        Assert.assertEquals("Wrong value", "B", result.get(1).getName());
        Assert.assertEquals("Wrong value", "C", result.get(2).getName());
        Assert.assertEquals("Wrong value", "D", result.get(3).getName());

        Assert.assertEquals("Wrong size", 2, result.get(0).getNodes().size());
        Assert.assertEquals("Wrong size", 0, result.get(1).getNodes().size());
        Assert.assertEquals("Wrong size", 1, result.get(2).getNodes().size());
        Assert.assertEquals("Wrong size", 1, result.get(3).getNodes().size());
    }
    
}
